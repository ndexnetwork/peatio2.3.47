# encoding: UTF-8
# frozen_string_literal: true
# This file is auto-generated from the current state of VCS.
# Instead of editing this file, please use bin/gendocs.

module Peatio
  class Application
    GIT_TAG =    '2.3.47'
    GIT_SHA =    '25d4a6a'
    BUILD_DATE = '2020-03-07 19:13:58+00:00'
    VERSION =    GIT_TAG
  end
end
