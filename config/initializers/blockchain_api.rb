Peatio::Blockchain.registry[:bitcoin] = Bitcoin::Blockchain.new
Peatio::Blockchain.registry[:geth] = Ethereum::Blockchain.new
Peatio::Blockchain.registry[:parity] = Ethereum::Blockchain.new
Peatio::Blockchain.registry[:nexbitd] = Nexbit::Blockchain.new
Peatio::Blockchain.registry[:ndcd] = Ndc::Blockchain.new
Peatio::Blockchain.registry[:ndx] = Ndex::Blockchain.new
